package com.zuitt;

import java.util.Scanner;

public class factorial {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a number:");
        int num = in.nextInt();

        int counter = 1;
        long answer = 1;

        try {
            if(num > 0){

            while (counter <= num){
                answer = answer * counter;
                counter++;
            }}
        } catch (Exception e){
            System.out.println("Invalid input");

        } finally {
            System.out.println("The factorial of number " + num + " is equal to " + answer);
        }
    }
}
